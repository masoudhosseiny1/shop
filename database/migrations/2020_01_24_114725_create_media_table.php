<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('image_address')->nullable();
            $table->string('video_address')->nullable();
            $table->enum('kind',['banner','product','slider','blog'])->nullable();
            $table->enum('main',['yes','no'])->nullable();
            $table->bigInteger('blog_id')->unsigned()->nullable();
            $table->foreign('blog_id')->references('id')->on('blogs');
            $table->bigInteger('product_id')->unsigned()->nullable();
            $table->enum('status',['active','disactive'])->nullable();
            $table->foreign('product_id')->references('id')->on('products');

            $table->timestamps();
    });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
