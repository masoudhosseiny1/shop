<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['name' => 'masoud', 'email' => 'hosseiny998@gmail.com', 'password' => bcrypt('123456789')],
            ['name' => 'masoud2', 'email' => 'hosseiny999@gmail.com', 'password' => bcrypt('123456789')],
        ];

        foreach ($users as $user) {
            \App\User::insert($user);
        }

        $products = [
            ['title' => 'dress ', 'description' => 'این لباس از پنبه ی خالص تهیه شده است',
                'mainprice' => '700000','offprice' => '55000', 'size' => 's', 'color' => 'blue'],
            ['title' => 'jeans ', 'description' => 'این لباس از کتان خالص تهیه شده است',
                'mainprice' => '600000', 'size' => 'xl', 'color' => 'green'],
            ['title' => 'tshirt ', 'description' => 'این تیشرت برای فصل های گرم مناسب است',
                'mainprice' => '400000', 'size' => 's', 'color' => 'red'],
            ['title' => 'hat ', 'description' => 'این کت برای فصل های سرد مناسب است',
                'mainprice' => '300000', 'size' => 's', 'color' => 'red'],
            ['title' => 'bag', 'description' => 'این لباس از پنبه ی خالص تهیه شده است',
                'mainprice' => '700000','offprice' => '55000', 'size' => 's', 'color' => 'blue'],
            ['title' => 'shoes ', 'description' => 'این لباس از کتان خالص تهیه شده است',
                'mainprice' => '600000', 'size' => 'xl', 'color' => 'green'],
            ['title' => 'overcoat ', 'description' => 'این تیشرت برای فصل های گرم مناسب است',
                'mainprice' => '400000', 'size' => 's', 'color' => 'red'],
            ['title' => 'coat ', 'description' => 'این کت برای فصل های سرد مناسب است',
                'mainprice' => '300000', 'size' => 's', 'color' => 'red'],
        ];
        foreach ($products as $product) {
            \App\Product::insert($product);
        }

        $categories = [
            ['title' => 'men', 'display_name' => 'مردانه','kind'=>'post'],
            ['title' => 'women', 'display_name' => 'زنانه','kind'=>'post'],
            ['title' => 'boy', 'display_name' => 'پسرانه','kind'=>'post'],
            ['title' => 'girl', 'display_name' => 'دخترانه','kind'=>'post'],
            ['title' => 'tech', 'display_name' => 'تکنولوژی','kind'=>'blog'],
            ['title' => 'political', 'display_name' => 'سیاسی','kind'=>'blog'],
        ];

        foreach ($categories as $category) {
            \App\Category::insert($category);
        }

        $categoryProduct = [
            ['category_id' => '2', 'product_id' => '1'],
            ['category_id' => '2', 'product_id' => '2'],
            ['category_id' => '2', 'product_id' => '3'],
            ['category_id' => '2', 'product_id' => '4'],
            ['category_id' => '1', 'product_id' => '5'],
            ['category_id' => '1', 'product_id' => '6'],
            ['category_id' => '1', 'product_id' => '7'],
            ['category_id' => '1', 'product_id' => '8'],
        ];
        foreach ($categoryProduct as $cat) {
            DB::table('category_product')->insert($cat);
        }

        $media = [
            ['image_address' => 'uploads\insta-1.jpg','kind' => 'product' ,'product_id' =>'3'],
            ['image_address' => 'uploads\insta-2.jpg','kind' => 'product' ,'product_id' =>'2'],
            ['image_address' => 'uploads\insta-3.jpg','kind' => 'product' ,'product_id' =>'1'],
            ['image_address' => 'uploads\insta-4.jpg','kind' => 'product' ,'product_id' =>'4'],
            ['image_address' => 'uploads\man-1.jpg','kind' => 'product' ,'product_id' =>'5'],
            ['image_address' => 'uploads\man-2.jpg','kind' => 'product' ,'product_id' =>'6'],
            ['image_address' => 'uploads\man-3.jpg','kind' => 'product' ,'product_id' =>'7'],
            ['image_address' => 'uploads\man-4.jpg','kind' => 'product' ,'product_id' =>'8'],
            ['title'=>'men','image_address' => 'uploads\banner-1.jpg','kind' => 'banner' ,'status'=>'active'],
            ['title'=>'women','image_address' => 'uploads\banner-2.jpg','kind' => 'banner' ,'status'=>'active'],
            ['title'=>'kid','image_address' => 'uploads\banner-3.jpg','kind' => 'banner' ,'status'=>'active'],
            ['title'=>'all','image_address' => 'uploads\banner-3.jpg','kind' => 'banner' ,'status'=>'active'],
            ['title'=>'Special Sale','image_address' => 'uploads/hero-2.jpg','kind' => 'slider'  ,'status'=>'active'],
            ['title'=>'Special Sale','image_address' => 'uploads/hero-3.jpg','kind' => 'slider' ,'status'=>'active'],
        ];
        foreach ($media as $med){
            \App\Media::insert($med);
        }

        $social = [
            ['title' => 'facebook' , 'name' => 'فیس بوک' , 'class_name' => 'ion-logo-facebook' , 'class_font_name'=> 'fa fa-facebook', 'network_path'=> 'facebook.com' ,'kind' => 'social','status' => 'active'],
            ['title' => 'twitter' , 'name' => 'توییتر' , 'class_name' => 'ion-logo-twitter' , 'class_font_name'=> 'fa fa-twitter', 'network_path'=> 'twitter.com' ,'kind' => 'social','status' => 'disactive'],
            ['title' => 'youtube' , 'name' => 'یوتیوب' , 'class_name' => 'ion-logo-youtube' , 'class_font_name'=> 'fa fa-youtube', 'network_path'=> 'youtube.com','kind' => 'social' ,'status' => 'active'],
            ['title' => 'email' , 'name' => 'ایمیل' , 'class_name' => 'ion-logo-newspaper' , 'class_font_name'=> 'fa fa-envelope', 'email'=> 'hosseiny998@gmail.com','kind' => 'contact' ,'status' => 'active'],
            ['title' => 'phone' , 'name' => 'تلفن' ,'phone_number' => '021-22334455', 'class_name' => 'ion-logo-phone' , 'class_font_name'=> 'fa fa-envelope','kind' => 'contact' ,'status' => 'active'],
            ['title' => 'address' , 'name' => 'آدرس' ,'address' => 'no 32,jax street,NewYork,USA', 'class_name' => 'ion-logo-phone' , 'class_font_name'=> 'fa fa-envelope','kind' => 'contact' ,'status' => 'active'],
        ];
        foreach ($social as $Social){
            \App\SocilNetwork::insert($Social);
        }
    }
}
