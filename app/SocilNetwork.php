<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocilNetwork extends Model
{
    protected $fillable=['title', 'name' ,'class','url','status'];
}
