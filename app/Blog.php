<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['title','description','body','status'];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
