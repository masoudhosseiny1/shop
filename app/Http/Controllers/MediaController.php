<?php

namespace App\Http\Controllers;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MediaController extends Controller
{
    public function AddMedia(){
        return view('AdminPage.Users.addmedia');
    }
    public function SubmitMedia(Request $request){
        $pathOfVideoFiles = 'uploads/';
        $storeFile = $request->file('propic')->move(public_path($pathOfVideoFiles), $request->file('propic')->getClientOriginalName());
        $updateDb = Media::create([
            'title'         => $request->title,
            'image_address' => $pathOfVideoFiles . $request->file('propic')->getClientOriginalName(),
            'kind'          => $request->imagekind,
            'status'        => 'active',
        ]);

        if ($storeFile && $updateDb){
            return redirect('/');
        }
    }
    public static  function ListOfBannerImage(){
        $GetBannerImage = Media::where('kind','banner')->where('status','active')->latest()->limit(3)->get();
        return $GetBannerImage;
    }
    public static  function ListOfSliderImage(){
        $GetSliderImage = Media::where('kind','slider')->where('status','active')->latest()->get();
        return $GetSliderImage;
    }

}
