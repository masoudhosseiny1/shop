<?php

namespace App\Http\Controllers;

use App\SocilNetwork;
use Illuminate\Http\Request;

class SocialNetworkController extends Controller
{
    public function ShowSocialPage()
    {
        $SocialDetails = SocilNetwork::all();
        return view('AdminPage.Users.socialnetwork', compact('SocialDetails'));
    }

    public function SocialSubmit(Request $request)
    {
        if ($request->checkbox == 'active') {
            $checkbox = 'active';
        } else {
            $checkbox = 'disactive';
        }
        $EditSocial = SocilNetwork::where('title', $request->socialtitle)->update([
            'network_path' => $request->socialurl,
            'status' => $checkbox,
        ]);

        if ($EditSocial) {
            return $this->ShowSocialPage();
        }
    }

    public static function SendSocial(){
        return SocilNetwork::where('status' , 'active')->get();
    }

    public function ShowContact(){
        $GetSocialNetwork = $this->SendSocial();
        return view('front.contact',compact('GetSocialNetwork'));
    }
}

