<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Media;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    //----------------BACK END---------

    public function ShowAddBlog(){
        $category = Category::all();
        return view('AdminPage.Users.addblog',compact('category'));
    }

    public function AddBlog(Request $request){
        $SubmitIntoDirectory = $request->file('blogpic')->move(public_path('uploads/'),
            $request->file('blogpic')->getClientOriginalName());

        $SubmitIntoDb = Blog::create([
            'title'        => $request->title,
            'description'  => $request->description,
            'body'         => $request->body,
        ]);
        $blogid = Blog::latest('id')->first()->id;

        $SubmitImageIntoDb = Media::create([
            'image_address' => 'uploads/'. $request->file('blogpic')->getClientOriginalName(),
            'kind'          => 'blog',
            'blog_id'       => $blogid,
        ]);

        $SubmitCategory = Blog::find($blogid);
        $SubmitCategory->categories()->sync($request->category);

        $switalert = 0 ;
        if ($SubmitIntoDb && $SubmitIntoDirectory && $SubmitImageIntoDb)
            $switalert = 1;
        $BlogPosts = Blog::latest()->get();
        return view('AdminPage.Users.bloglist',compact('BlogPosts'));
    }

    //---------FRONT END--------------

    public function BlogImage($id){
        return $GetBlogImage = Media::where('blog_id',$id)->get();
    }
    public function BlogImages(){
        return $GetBlogImages = Media::where('kind','blog')->latest()->get();
    }
    public function BlogDetail($id){
        return $GetBlogDetail = Blog::where('id',$id)->get();
    }

    public function BlogDetailPreviousPost($id){
        return $GetBlogDetail = Blog::where('id',$id - 1 )->get();
    }

    public function BlogDetailNextPost($id){
        return $GetBlogDetail = Blog::where('id',$id + 1 )->get();
    }

    public function ShowBlog(){
        $GetSocialNetwork = SocialNetworkController::SendSocial();
        $GetBlogImages    = $this->BlogImages();
        $BlogPosts        = Blog::latest()->get();
        return view('front.bloglist',compact('GetSocialNetwork','BlogPosts','GetBlogImages'));
    }
    public function ShowBlogPage($id){
        $GetSocialNetwork           = SocialNetworkController::SendSocial();
        $GetBlogImage               = $this->BlogImage($id);
        $GetBlogDetail              = $this->BlogDetail($id);
        $GetBlogDetailPreviousPost  = $this->BlogDetailPreviousPost($id);
        $GetBlogDetailNextPost      = $this->BlogDetailNextPost($id);

        return view('front.blogsinglepage',compact('GetSocialNetwork','GetBlogImage','GetBlogDetail','GetBlogDetailNextPost','GetBlogDetailPreviousPost'));
    }

    public function BlogList(){
        $BlogPosts        = Blog::latest()->get();
        return view('AdminPage.Users.bloglist',compact('BlogPosts'));
    }
    public static function Blog3Posts(){
        return $Blog3Posts = Blog::latest()->get();
    }
    public static function Blog3PostsImage(){
        return $Blog3PostsImage = Media::where('kind','blog')->get();
    }
}
