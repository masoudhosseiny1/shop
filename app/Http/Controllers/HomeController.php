<?php

namespace App\Http\Controllers;

use App\Mail\SendMailable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\ProductController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//
//    public function mail(){
//        $name = 'کاربر جدید';
//        Mail::to('hosseiny998@gmail.com')->send(new SendMailable($name));
//
//        return 'email was sent';
//    }
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public static function StoreImage($file,$path = 'uploads/'){
        $StoreFile = $file->move(public_path($path),$file->getClientOriginalName());
    }
}
