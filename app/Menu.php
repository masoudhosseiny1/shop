<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['title','parent','status','sort'];

    public function sub_category(){
        return $this->hasMany(self::class,'parent','id');
    }
}
