<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class Product extends Model
{
    protected $fillable=['title','description','mainprice','offprice','totalprice','size','color'];

    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function ProductImage(){
        return $this->hasMany('Media');
    }
}
