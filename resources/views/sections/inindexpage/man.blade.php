
<section class="women-banner spad">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-8 offset-lg-1">
                <div class="filter-control">
                    <ul>
                        <li class="active">cloths</li>
                        <li>bags</li>
                        <li>shoes</li>
                        <li>others</li>
                    </ul>
                </div>
                <div class="product-slider owl-carousel">
                    @if(!isset(auth()->user()->role))
                        {{--//------------NOT REGISTERED----------}}
                        @foreach($GetMenProduct as $catt)
                            @foreach($catt->products as $pro)

                                <div class="product-item">
                                    <div class="pi-pic">
                                        @foreach($GetProductImage->where('product_id',$pro->id)->where('kind','product')  as $image)
                                            <img src="{{$image->image_address}}" alt="">
                                        @endforeach
                                        @if($pro->offprice)
                                            <div class="sale">off</div>
                                        @endif
                                        <div class="icon">
                                            <i class="icon_heart_alt"></i>
                                        </div>
                                        <ul>
                                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                                            <li class="quick-view"><a href="#">view</a></li>
                                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="pi-text">
                                        <div class="catagory-name">{{$catt->display_name}}</div>
                                        <a href="ListOfProduct">
                                            <h5>{{ $pro->title }}</h5>
                                            {{$pro->media_id}}
                                        </a>
                                        <div class="product-price" title="to view prices login please">hidden price
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach

                    @elseif(isset(auth()->user()->role) && auth()->user()->role == 'haghighi')
                        {{--//--------------HAGHIHGI-------------}}
                        @foreach($GetMenProduct as $catt)
                            @foreach($catt->products->where('offprice',Null) as $pro)

                                <div class="product-item">
                                    <div class="pi-pic">
                                        @foreach($GetProductImage->where('product_id',$pro->id)->where('kind','product')  as $image)
                                            <img src="{{$image->image_address}}" alt="">
                                        @endforeach
                                        <div class="icon">
                                            <i class="icon_heart_alt"></i>
                                        </div>
                                        <ul>
                                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                                            <li class="quick-view"><a href="#"> view</a></li>
                                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="pi-text">
                                        <div class="catagory-name">{{$catt->display_name}}</div>
                                        <a href="ListOfProduct">
                                            <h5>{{ $pro->title }}</h5>
                                            {{$pro->media_id}}
                                        </a>
                                        <div class="product-price">
                                            {{$pro->mainprice}}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach

                    @elseif(isset(auth()->user()->role) && auth()->user()->role == 'hoghooghi')
                        {{--//----------HOGHOOGHI---------------}}
                        @foreach($GetMenProduct as $catt)
                            @foreach($catt->products->where('offprice',!Null) as $pro)

                                <div class="product-item">
                                    <div class="pi-pic">
                                        @foreach($GetProductImage->where('product_id',$pro->id)->where('kind','product')  as $image)
                                            <img src="{{$image->image_address}}" alt="">
                                        @endforeach
                                        @if($pro->offprice)
                                            <div class="sale">off</div>
                                        @endif
                                        <div class="icon">
                                            <i class="icon_heart_alt"></i>
                                        </div>
                                        <ul>
                                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                                            <li class="quick-view"><a href="#">view </a></li>
                                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="pi-text">
                                        <div class="catagory-name">{{$catt->display_name}}</div>
                                        <a href="ListOfProduct">
                                            <h5>{{ $pro->title }}</h5>
                                            {{$pro->media_id}}
                                        </a>

                                        @if($pro->offprice)
                                            <div class="product-price">
                                                {{$pro->offprice}}
                                                <br>
                                                <span>{{$pro->mainprice}}</span>
                                            </div>
                                        @else
                                            <div class="product-price">
                                                {{$pro->mainprice}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    @else
                        {{--//-------------ADMIN-----------}}
                        @foreach($GetMenProduct as $catt)
                            @foreach($catt->products as $pro)

                                <div class="product-item">
                                    <div class="pi-pic">
                                        @foreach($GetProductImage->where('product_id',$pro->id)->where('kind','product')  as $image)
                                            <img src="{{$image->image_address}}" alt="">
                                        @endforeach
                                        @if($pro->offprice)
                                            <div class="sale">off</div>
                                        @endif
                                        <div class="icon">
                                            <i class="icon_heart_alt"></i>
                                        </div>
                                        <ul>
                                            <li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>
                                            <li class="quick-view"><a href="#">view </a></li>
                                            <li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="pi-text">
                                        <div class="catagory-name">{{$catt->display_name}}</div>
                                        <a href="ListOfProduct">
                                            <h5>{{ $pro->title }}</h5>
                                            {{$pro->media_id}}
                                        </a>

                                        @if($pro->offprice)
                                            <div class="product-price">
                                                {{$pro->offprice}}
                                                <br>
                                                <span>{{$pro->mainprice}}</span>
                                            </div>
                                        @else
                                            <div class="product-price">
                                                {{$pro->mainprice}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    @endif



                    {{----}}
                    {{--@foreach($GetWomenProduct as $catt)--}}
                    {{--@foreach($catt->products as $pro)--}}

                    {{--<div class="product-item">--}}
                    {{--<div class="pi-pic">--}}
                    {{--@foreach($GetProductImage->where('product_id',$pro->id)->where('kind','product')  as $image)--}}
                    {{--<img src="{{$image->image_address}}" alt="">--}}
                    {{--@endforeach--}}
                    {{--@if($pro->offprice)--}}
                    {{--<div class="sale">فروش ویژه</div>--}}
                    {{--@endif--}}
                    {{--<div class="icon">--}}
                    {{--<i class="icon_heart_alt"></i>--}}
                    {{--</div>--}}
                    {{--<ul>--}}
                    {{--<li class="w-icon active"><a href="#"><i class="icon_bag_alt"></i></a></li>--}}
                    {{--<li class="quick-view"><a href="#">نمایش سریع</a></li>--}}
                    {{--<li class="w-icon"><a href="#"><i class="fa fa-random"></i></a></li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--<div class="pi-text">--}}
                    {{--<div class="catagory-name">{{$catt->display_name}}</div>--}}
                    {{--<a href="ListOfProduct">--}}
                    {{--<h5>{{ $pro->title }}</h5>--}}
                    {{--{{$pro->media_id}}--}}
                    {{--</a>--}}

                    {{--@if($pro->offprice)--}}
                    {{--<div class="product-price">--}}
                    {{--{{$pro->offprice}}--}}
                    {{--<br>--}}
                    {{--<span>{{$pro->mainprice}}</span>--}}
                    {{--</div>--}}
                    {{--@else--}}
                    {{--<div class="product-price">--}}
                    {{--{{$pro->mainprice}}--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                    {{--@endforeach--}}
                </div>
            </div>

            <div class="col-lg-3">
                <div class="product-large set-bg" data-setbg="img/products/man-large.jpg">
                    <h2>men's</h2>
                    <a href="#">see more</a>
                </div>
            </div>
        </div>
    </div>
</section>
