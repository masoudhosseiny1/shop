<div class="banner-section spad">
        <div class="container-fluid">
            <div class="row">
                @foreach($GetBannerImage as $image)
                <div class="col-lg-4">
                    <div class="single-banner">
                        <img src="{{$image->image_address}}" alt="">
                        <div class="inner-text">
                            <h4>{{$image->title}}</h4>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
