@extends('AdminPage.Master')

@section('script')
    <script>
        $(document).ready(function () {
            $(this).find("#permission_id").selectpicker();
        });
    </script>
@endsection
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    function  alertt() {
        Swal.fire({
            position: 'bottom-end',
            icon: 'success',
            title: 'مقاله شما با موفقیت ثبت شد',
            showConfirmButton: false,
            timer: 7000
        })
    }
</script>
@section('content')
    <div class="content-wrapper" style="margin-top: 20px">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title" style="font:15px IranSans !important; text-align: center">ایجاد مقاله جدید </h3>
                                @if($switalert ?? '' ==1)
                                <script>alertt();</script>
                                    @endif
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('addblog')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{--@include('AdminPage.Errors')--}}
                                    <div class="form-group">
                                        <div class="col-sm-12" style="text-align: right">
                                            <label for="name" class="control-label">عنوان مقاله</label>
                                            <input type="text" class="form-control" name="title" id="name" placeholder="عنوان محصول " value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12" style="text-align: right">
                                            <label for="permission_id" class="control-label">توضیح مختصر مقاله</label>
                                            <textarea name="description" class="form-control" placeholder="توضیحات مقاله را وارد کنید"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12" style="text-align: right">
                                            <label for="permission_id" class="control-label">متن اصلی مقاله</label>
                                            <textarea style="height: 25em;" name="body" class="form-control" placeholder="بدنه ی مقاله"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group" style="text-align: right">
                                        <div class="col-sm-12">
                                            <label for="label" class="control-label">عکس مقاله</label>

                                         <input type="file" name="blogpic">
                                            <hr>
                                        </div>
                                    </div>
                                    <div class="form-group" style="text-align: right">
                                        <div class="col-sm-12">
                                            <label for="label" class="control-label">دسته بندی مقاله</label>
                                            <select name="category" class="form-control">
                                                @foreach($category->where('kind','blog') as $cat)
                                                <option value="{{$cat->id}}">{{$cat->title}}</option>
                                                @endforeach
                                            </select>
                                            <hr>
                                        </div>
                                    </div>

                                    <div class="form-group" >
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success" style="width: 250px !important; margin-right: 780px !important;"> ثبت مقاله </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.card -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section>
        </div><!-- /.content -->
    </div>

    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>

@endsection
