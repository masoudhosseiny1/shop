@extends('AdminPage.Master')

@section('content')

    <div class="content-wrapper" style="margin-top: 20px">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-hover table-dark text-center">
                                    <thead>
                                    <tr>
                                        <th scope="col">ردیف</th>
                                        <th scope="col">عنوان</th>
                                        <th scope="col">توضیح کوتاه</th>
                                    </tr>
                                    </thead>
                                    {{$Number =1}}
                                    <tbody>
                                    @foreach($BlogPosts as $Blog)
                                        <tr>
                                            <th scope="row">{{$Number++}}</th>
                                            <td>{{$Blog->title}}</td>
                                            <td>{{$Blog->description}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div><!-- /.card-body -->
                        </div><!-- /.card -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section>
            <div style="text-align: center">
            </div>
        </div><!-- /.content -->
    </div>

@endsection()
