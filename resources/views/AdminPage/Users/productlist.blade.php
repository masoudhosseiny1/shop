@extends('AdminPage.Master')

@section('content')

    <div class="content-wrapper" style="margin-top: 20px">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-hover table-dark text-center">
                                    <thead>
                                    <tr>
                                        <th scope="col">ردیف</th>
                                        <th scope="col">نام محصول</th>
                                        <th scope="col">قیمت اصلی</th>
                                    </tr>
                                    </thead>
                                    {{$Number =1}}
                                    <tbody>
                                    @foreach($AllProduct as $product)
                                        <tr>
                                            <th scope="row">{{$Number++}}</th>
                                            <td>{{$product->title}}</td>
                                            <td>{{$product->mainprice}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>


                            </div><!-- /.card-body -->
                        </div><!-- /.card -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section>
            <div style="text-align: center">
            </div>
        </div><!-- /.content -->
    </div>

@endsection()
