@extends('AdminPage.Master')

@section('script')
    <script>
        $(document).ready(function () {
            $(this).find("#permission_id").selectpicker();
        });
    </script>
@endsection
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    function  alertt() {
        Swal.fire({
            position: 'bottom-end',
            icon: 'success',
            title: 'شبکه اجتماعی شما با موفقیت تغییر کرد شد',
            showConfirmButton: false,
            timer: 7000
        })
    }
</script>
@section('content')
    <div class="content-wrapper" style="margin-top: 20px">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title" style="font:15px IranSans !important; text-align: center">مدیریت شبکه های اجتماعی </h3>
                                {{--@if($switalert ==1)--}}
                                {{--<script>alertt();</script>--}}
                                    {{--@endif--}}
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                @foreach($SocialDetails as $Social)
                                <form action="socialsubmit" method="post">
                                    @csrf

                                <div class="text-right">
                                    <h4><i class="{{$Social->class_name}} fa-2x"></i>{{$Social->name}} : </h4>
                                    <br>
                                    <label> آدرس : </label>
                                    <input type="text" name="socialurl" value="{{$Social->network_path}}">
                                    <label>فعال کردن</label>
                                    <input type="checkbox" name="checkbox" value="active" @if($Social->status == 'active'){{$checkbox = 'checked'}}@endif>
                                    <input type="text" name="socialtitle" value="{{$Social->title}}" hidden>
                                    <input type="submit" value="ثبت تغییرات {{$Social->name}}" class="btn btn-success">
                                </div>
                                </form>
                                @endforeach
                            </div>
                        </div><!-- /.card -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section>
        </div><!-- /.content -->
    </div>

    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>

@endsection
