@extends('AdminPage.Master')

@section('script')
    <script>
        $(document).ready(function () {
            $(this).find("#permission_id").selectpicker();
        });
    </script>
@endsection
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    function  alertt() {
        Swal.fire({
            position: 'bottom-end',
            icon: 'success',
            title: 'محصول شما با موفقیت ثبت شد',
            showConfirmButton: false,
            timer: 7000
        })
    }
</script>
@section('content')
    <div class="content-wrapper" style="margin-top: 20px">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title" style="font:15px IranSans !important; text-align: center">آپلود یک عکس جدید</h3>
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <form action="{{route('submitmedia')}}" method="post" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    {{--@include('AdminPage.Errors')--}}
                                    <div class="form-group">
                                        <div class="col-sm-12" style="text-align: right">
                                            <label for="label" class="control-label">عنوان عکس</label>
                                            <input type="text" class="form-control" name="title" id="name" placeholder="عنوان محصول " value="">
                                        </div>
                                    </div>
                                    <div class="form-group" style="text-align: right">
                                        <div class="col-sm-12">
                                            <label for="label" class="control-label">سایز محصول</label>
                                            <select name="imagekind" class="form-control">
                                                <option value="slider">slider</option>
                                                <option value="banner">banner</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" style="text-align: right">
                                        <div class="col-sm-12">
                                            <label for="label" class="control-label">انتخاب عکس</label>
                                            <hr>
                                         <input type="file" name="propic">
                                            <hr>
                                        </div>
                                    </div>

                                    <div class="form-group" >
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success" style="width: 250px !important; margin-right: 780px !important;"> ثبت عکس </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div><!-- /.card -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </section>
        </div><!-- /.content -->
    </div>

    <script>
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>

@endsection
