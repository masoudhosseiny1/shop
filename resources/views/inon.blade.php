<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fashi | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="cssfront/app.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
   @include('sections.headerfront')
    <!-- Header End -->

    <!-- Hero Section Begin -->
    @include('sections.inindexpage.slider')
    <!-- Hero Section End -->

    <!-- Banner Section Begin -->
    @include('sections.inindexpage.3cat')
    <!-- Banner Section End -->

    <!-- Women Banner Section Begin -->
    @include('sections.inindexpage.women')

    <!-- Women Banner Section End -->

    <!-- Deal Of The Week Section Begin-->
    @include('sections.inindexpage.dealoftheweek')
    <!-- Deal Of The Week Section End -->

    <!-- Man Banner Section Begin -->
    @include('sections.inindexpage.man')
    <!-- Man Banner Section End -->

    <!-- Instagram Section Begin -->
    @include('sections.inindexpage.instagram')
    <!-- Instagram Section End -->

    <!-- Latest Blog Section Begin -->
    @include('sections.inindexpage.blog')
    <!-- Latest Blog Section End -->

    <!-- Partner Logo Section Begin -->
    @include('sections.inindexpage.logos')
    <!-- Partner Logo Section End -->

    <!-- Footer Section Begin -->
        @include('sections.footerfront')
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="jsfront/app.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>
</html>
