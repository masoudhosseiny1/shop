<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Fashi Template">
    <meta name="keywords" content="Fashi, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fashi | Template</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="../cssfront/app.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('sections.headerfront')
    <!-- Header End -->

    <!-- Blog Details Section Begin -->
    <section class="blog-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-details-inner">
                        <div class="blog-detail-title">
                            @foreach($GetBlogDetail as $Blog)
                            <h2>{{$Blog->title}}</h2>
                            {{--<p>travel <span>- May 19, 2019</span></p>--}}
                            @endforeach
                        </div>
                        <div class="blog-large-pic">
                            @foreach($GetBlogImage as $Image)
                            <img src="/{{$Image->image_address}}">
                            @endforeach
                        </div>
                        <div style="text-align: right">
                            @foreach($GetBlogDetail as $body)
                                <br>
                                <br>
                            <p>
                                {{$body->body}}
                            </p>

                        </div>
                        @endforeach
                        <div class="tag-share">
                            <div class="details-tag">
                                <ul>
                                    <li><i class="fa fa-tags"></i></li>
                                    <li>Travel</li>
                                    <li>Beauty</li>
                                    <li>Fashion</li>
                                </ul>
                            </div>
                            <div class="blog-share">
                                <span>Share:</span>
                                <div class="social-links">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-youtube-play"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="blog-post">
                            <div class="row">
                                @foreach($GetBlogDetailPreviousPost as $previous)
                                <div class="col-lg-5 col-md-6">
                                    <a href="../blog/{{$previous->id}}" class="prev-blog">
                                        <div class="pb-pic">
                                            <i class="ti-arrow-left"></i>
                                            <img src="../../../../../stedeo%20farsi/shop/Online%20Shop/img/blog/prev-blog.png" alt="">
                                        </div>
                                        <div class="pb-text">
                                            <span>Previous Post:</span>
                                            <h5>{{$previous->title}}</h5>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                                @foreach($GetBlogDetailNextPost as $next)
                                <div class="col-lg-5 offset-lg-2 col-md-6">
                                    <a href="../blog/{{$next->id}}" class="next-blog">
                                        <div class="nb-pic">
                                            <img src="../../../../../stedeo%20farsi/shop/Online%20Shop/img/blog/next-blog.png" alt="">
                                            <i class="ti-arrow-right"></i>
                                        </div>
                                        <div class="nb-text">
                                            <span>Next Post:</span>
                                            <h5>{{$next->title}}</h5>
                                        </div>
                                    </a>
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="posted-by">
                            <div class="pb-pic">
                                <img src="../../../../../stedeo%20farsi/shop/Online%20Shop/img/blog/post-by.png" alt="">
                            </div>
                            <div class="pb-text">
                                <a href="#">
                                    <h5>Shane Lynch</h5>
                                </a>
                                <p>Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                    velit esse cillum bore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                    amodo</p>
                            </div>
                        </div>
                        <div class="leave-comment">
                            <h4>Leave A Comment</h4>
                            <form action="#" class="comment-form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="text" placeholder="Name">
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" placeholder="Email">
                                    </div>
                                    <div class="col-lg-12">
                                        <textarea placeholder="Messages"></textarea>
                                        <button type="submit" class="site-btn">Send message</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Details Section End -->

    <!-- Partner Logo Section Begin -->
    @include('sections.inindexpage.logos')
    <!-- Partner Logo Section End -->

    <!-- Footer Section Begin -->
    @include('sections.footerfront')
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="../jsfront/app.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>
